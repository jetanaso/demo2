import warnings
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
import mlflow
warnings.filterwarnings("ignore")

################
# READ DATA IN #
################

df = pd.read_csv("mnist_data.csv")

####################
# TRAIN TEST SPLIT #
####################
X = df[df.columns[df.columns.str.startswith("pixel")]]
y = df["class"]

X_train, X_test, y_train, y_test = train_test_split(X,
                                                    y,
                                                    test_size=0.2,
                                                    random_state=2023,
                                                    stratify=y)

#####################
# STANDARDIZED DATA #
#####################

scaler = StandardScaler()
scaler.fit(X_train)

X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)

#######################
# LOGISTIC REGRESSION #
#######################

mlflow.set_tracking_uri("http://localhost:5000")
mlflow.set_experiment("demo2")

mlflow.autolog(log_model_signatures=True, log_input_examples=True)

params = ["lbfgs", "liblinear", "newton-cg", "newton-cholesky", "sag", "saga"]
with mlflow.start_run(run_name="main_run") as run:
    for param in params:
        with mlflow.start_run(run_name=f"{param}", nested=True) as nested:
            model = LogisticRegression(solver=param)
            model.fit(X_train_scaled, y_train)

            y_pred = model.predict(X_test_scaled)

            accuracy = metrics.accuracy_score(y_test, y_pred)
            precision = metrics.precision_score(y_test, y_pred, average="weighted")
            recall = metrics.recall_score(y_test, y_pred, average="weighted")
            mlflow.log_metrics({"testing_accuracy": accuracy,
                                "testing_precision": precision,
                                "testing_recall": recall})
