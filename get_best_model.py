import os
import pandas as pd
import mlflow
from rich import print

pd.options.display.max_columns = 999999
os.environ["MLFLOW_TRACKING_URI"] = "sqlite:///mlflow.db"

df = mlflow.search_runs(search_all_experiments=True,
                        filter_string="metrics.testing_accuracy > 0.88")
print(df[:1].T)

run_id = df.loc[df["metrics.testing_accuracy"].idxmax()]["run_id"]
best_model = mlflow.sklearn.load_model("runs:/" + run_id + "/model")
print()
print("The best model based on RMSE is", best_model)
