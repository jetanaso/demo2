import os
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import mlflow.pyfunc

np.set_printoptions(suppress=True)
os.environ["MLFLOW_TRACKING_URI"] = "sqlite:///mlflow.db"

model_name = "main_classifier"
model_version = "1"

logged_model = f"models:/{model_name}/{model_version}"
loaded_model = mlflow.pyfunc.load_model(logged_model)
sklearn_model = mlflow.sklearn.load_model(logged_model)

df = pd.read_csv("mnist_data.csv")

#####################################################################

X = df[df.columns[df.columns.str.startswith("pixel")]]
y = df["class"]

X_train, X_test, y_train, y_test = train_test_split(X,
                                                    y,
                                                    test_size=0.2,
                                                    random_state=2023,
                                                    stratify=y)

scaler = StandardScaler()
scaler.fit(X_train)

#####################################################################

new_data = df.sample(1)
y_true = new_data["class"].squeeze()
print("true", y_true)

new_data = new_data.drop(columns=["class"])
new_data_scaled = scaler.transform(new_data)
y_pred = loaded_model.predict(new_data_scaled)[0]
y_pred_proba = sklearn_model.predict_proba(new_data_scaled)
print("prediction", y_pred)
print("probability", y_pred_proba)

plt.matshow(new_data.to_numpy().reshape(28, -1))
plt.show()
